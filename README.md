# Cortopia_Command

A missile command like game developed in Unity as a code test for Cortopia Studios.

Player launches missile by moving the mouse and clicking on the screen. This will use the closest missile launcher to fire a missile if it has any left.

The objective of the game is to survive the attacker onslaught by keeping the cities alive while the attacker fires missiles and bombs towards the ground.

## Normal Mode
If the player loses all cities they lose, however if even one city remains and the attacker runs out of missiles and bombs they claim victory and earn an amount of points based on how many opposing missiles and bombs they managed to destroy.

Points are tracked by chain score and how many cities are left alive modified by difficulty.
In easy mode, player missiles perpetually regenerate every 5 seconds, for medium and hard the player has a limited amount of missiles.
The attacker has a limited amount of missiles and bombs which will be fired towards the cities at random.

## Classic Mode
In this mode, the aim is to survive for as long as possible with missiles only regenerating after acquiring at least 400 points in score.

Points are tracked by chain score, how many cities are left and survival time.

--------------------------------------------------------------------------------

- Audio has all been sourced from https://freesound.org
- Font was sourced from https://www.dafont.com/es/theme.php?cat=402
- City model was sourced from https://www.turbosquid.com/3d-models/free-obj-model-city-scifi-fantasy/788958
- Rocket Launcher model was sourced from https://www.turbosquid.com/3d-models/ready-rocket-launcher-max-free/723261
- Weighted selection was based on this project: https://github.com/kinetiq/Ether.WeightedSelector
