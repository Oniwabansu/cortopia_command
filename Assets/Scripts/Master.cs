﻿using UnityEngine;

public class Master : MonoBehaviour
{
    public static Master Instance { get; private set; }
    
    public CommanderConfig defenderSetup;
    public CommanderConfig attackerSetup;

    public ResourcePool poolManager;

    public ScoreManager scoreManager;
    public EndGameUI endGameUI;
    public GameObject gameUI;

    public TurretCommand turretCommand;
    public EnemyCommand enemyCommand;

    public bool GameActive
    {
        get;
        private set;
    } = true;

    private void Awake()
    {
        try
        {
            if (Instance != null) { throw new System.SystemException("Can't create more than one instance of Master"); }

            Instance = this;

            scoreManager = new ScoreManager();

            if (poolManager == null)
            {
                if ((poolManager = GetComponent<ResourcePool>()) == null)
                {
                    throw new System.NullReferenceException("Please create an instance of pool before continuing");
                }
            }

            if (endGameUI == null)
            {
                if ((endGameUI = GetComponent<EndGameUI>()) == null)
                {
                    throw new System.NullReferenceException("Please create an instance of end game before continuing");
                }
            }
        }
        catch (System.Exception e)
        {
            Debug.LogError(e.Message);
            Destroy(this);
        }
    }

    int cityLeftCount = -1;

    // Start is called before the first frame update
    void Start()
    {
        City[] cities = FindObjectsOfType<City>();
        cityLeftCount = cities.Length;

        for (int i = 0; i < cities.Length; ++i)
        {
            cities[i].onDestroyed += () => 
            {
                if (--cityLeftCount == 0)
                {
                    EndGame();
                }
            };
        }
    }

    public void EndGame()
    {
        string endGameText;
        if (cityLeftCount > 0)
        {
            endGameText = "Victory";
        }
        else
        {
            endGameText = "Game Over - All cities have been lost";
        }

        GameActive = false;

        gameUI.SetActive(false);
        endGameUI.gameObject.SetActive(true);
        endGameUI.ShowEndGameUI(endGameText, scoreManager.GetTotalScore(), scoreManager.GetCityScore(), scoreManager.GetChainScore(), scoreManager.GetTimeScore());
    }
}
