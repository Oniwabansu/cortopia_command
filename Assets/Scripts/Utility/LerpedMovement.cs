﻿using System.Collections;
using UnityEngine;

public class LerpedMovement : MonoBehaviour
{
    private Vector2 initialPos, targetPos;
    private bool interrupted = false;

    public void Launch(Vector2 initialPos, Vector2 targetPos, float missileSpeed, System.Action onInterrupted)
    {
        transform.position = initialPos;
        this.initialPos = initialPos;
        this.targetPos = targetPos;

        StartCoroutine(Launch(missileSpeed, onInterrupted));
    }

    private void OnDisable()
    {
        interrupted = false;
    }

    private IEnumerator Launch(float missileSpeed, System.Action onInterrupt)
    {
        float time = 0;
        float distance = (targetPos - initialPos).magnitude;
        while ((Vector2)transform.position != targetPos && !interrupted)
        {
            transform.position = Vector3.Lerp(initialPos, targetPos, time);
            time += Time.deltaTime * missileSpeed / distance;

            yield return null;
        }

        interrupted = false;
        onInterrupt.Invoke();
    }

    public void Interrupt()
    {
        interrupted = true;
    }
}
