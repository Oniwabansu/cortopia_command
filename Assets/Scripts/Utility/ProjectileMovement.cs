﻿using System.Collections;
using UnityEngine;

public class ProjectileMovement : MonoBehaviour
{
    readonly float gravity = -0.981f; // correct value of -9.81 was too much for objects falling straight down so only using 1/10th of gravity
    
    private Vector2 initialPos;
    private bool interrupted = false;

    public void Launch(Vector2 initialPos, float angle, float velocity, System.Action onInterrupted)
    {
        transform.position = initialPos;
        this.initialPos = initialPos;
        float vx = velocity * Mathf.Cos(angle * Mathf.Deg2Rad);
        float vy = velocity * Mathf.Sin(angle * Mathf.Deg2Rad);

        StartCoroutine(Launch(vx, vy, onInterrupted));
    }

    private void OnDisable()
    {
        interrupted = false;
    }

    private IEnumerator Launch(float vx, float vy, System.Action onInterrupt)
    {
        float t = 0;
        while (!interrupted)
        {
            t += Time.deltaTime;

            float x = initialPos.x + vx * t;
            float y = initialPos.y -((vy * t) - (gravity / 2) * t * t);

            transform.position = new Vector2(x, y);
            yield return null;
        }

        interrupted = false;
        onInterrupt?.Invoke();
    }
    
    public void Interrupt()
    {
        interrupted = true;
    }
}
