﻿public class PoolDoesntExistException : System.NullReferenceException
{
    public PoolDoesntExistException(System.Type type) : base($"Pool of type {type.ToString()} hasn't been created yet, please do so before trying to use it")
    {

    }
}

public class PoolEmptyException : System.ArgumentOutOfRangeException
{
    public PoolEmptyException(System.Type type) : base($"Pool of type {type.ToString()} is empty and is set to not expand, please return objects to pool first")
    {

    }
}