﻿using System.Collections.Generic;
using UnityEngine;

public class ResourcePool : MonoBehaviour
{
    public PoolObject[] pools;

    private void Awake()
    {
        for (int i = 0; i < pools.Length; ++i)
        {
            if (pools[i].template == null || pools[i].initialCount == 0) { continue; }

            pools[i].pool = new Queue<PoolableObject>();

            pools[i].type = pools[i].template.GetType();
            for (int j = 0; j < pools[i].initialCount; ++j)
            {
                PoolableObject obj = Instantiate<PoolableObject>(pools[i].template);
                obj.transform.position = new Vector3(0, -500, 0);
                obj.gameObject.SetActive(false);
                obj.transform.SetParent(this.transform);
                pools[i].pool.Enqueue(obj);
            }
        }
    }
    
    public T GetPoolObj<T>() where T : PoolableObject
    {
        for (int i = 0; i < pools.Length; ++i)
        {
            if (pools[i].type == typeof(T))
            {
                if (pools[i].pool.Count > 0)
                {
                    T obj = pools[i].pool.Dequeue() as T;
                    obj.gameObject.SetActive(true);

                    return obj;
                }
                else if (pools[i].expandable)
                {
                    T obj = Instantiate<T>(pools[i].template as T);
                    obj.transform.SetParent(this.transform);

                    return obj;
                }
                else
                {
                    throw new PoolEmptyException(typeof(T));
                }
            }
        }
        throw new PoolDoesntExistException(typeof(T));
    }

    public void ReturnToPool<T>(T obj) where T : PoolableObject
    {
        for (int i = 0; i < pools.Length; ++i)
        {
            if (pools[i].type == typeof(T))
            {
                obj.gameObject.SetActive(false);
                obj.transform.position = new Vector3(0, -500, 0);
                pools[i].pool.Enqueue(obj);

                return;
            }
        }
        throw new PoolDoesntExistException(typeof(T));
    }
}

[System.Serializable]
public class PoolObject
{
    public System.Type type;

    public int initialCount;
    public bool expandable;

    public PoolableObject template;
    [HideInInspector]
    public Queue<PoolableObject> pool;

    public PoolObject()
    {
        pool = new Queue<PoolableObject>();
    }
}