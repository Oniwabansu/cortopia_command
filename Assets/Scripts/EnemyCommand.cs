﻿using System.Collections.Generic;
using UnityEngine;

public class EnemyCommand : MonoBehaviour
{
    private System.Random randomizer;

    private City[] targets;

    private int timeForNextExplosive;
    public int missilesLeft = 0;
    public int bombsLeft = 0;

    private int activeMissileRefCounter = 0;

    private WeightedSelector citySelector;
    private WeightedSelector explosiveSelector;

    public SelectorOptions weightedSelectorOptions;
    List<IWeightedItem> missileSelection = new List<IWeightedItem>();

    private float startTime = 0f;

    private void Awake()
    {
        citySelector = new WeightedSelector(weightedSelectorOptions);
        explosiveSelector = new WeightedSelector(weightedSelectorOptions);

        randomizer = new System.Random();
        timeForNextExplosive = randomizer.Next(1, 3);
        targets = FindObjectsOfType<City>();

        citySelector.SetFilteredItems(targets);
        
        missilesLeft = randomizer.Next(2, 5) + Ruleset.Instance.Difficulty.attacker_missilesAvailable;
        bombsLeft = Ruleset.Instance.Difficulty.attacker_bombsAvailable;

        startTime = Time.realtimeSinceStartup;
    }

    private void Start()
    {
        // add explosives to weighted selector from the pool templates
        for (int i = 0; i < Master.Instance.poolManager.pools.Length; ++i)
        {
            if (Master.Instance.poolManager.pools[i].type.BaseType == typeof(ExplosiveBase))
            {
                missileSelection.Add(Master.Instance.poolManager.pools[i].template as ExplosiveBase);
            }
        }

        explosiveSelector.SetFilteredItems(missileSelection.ToArray());
    }

    // Update is called once per frame
    void Update()
    {
        if (!Master.Instance.GameActive) { return; }

        if (Ruleset.Instance.Difficulty.eMode == GameMode.NORMAL && missilesLeft == 0 && bombsLeft == 0)
        {
            if (activeMissileRefCounter == 0)
            {
                Master.Instance.EndGame();
            }
            return;
        }
        if (Time.realtimeSinceStartup - startTime > timeForNextExplosive)
        {
            // set a random timer for next missile to appear
            timeForNextExplosive += randomizer.Next(1, 3) + Ruleset.Instance.Difficulty.attacker_firingSpeedModifier;

            // select next explosive based on weight
            IWeightedItem explosive = explosiveSelector.Select();
            switch (explosive.GetType().FullName)
            {
                case nameof(Missile):
                default:
                    {
                        Missile missile = Master.Instance.poolManager.GetPoolObj<Missile>();
                        Vector2 originVector = new Vector2(randomizer.Next(-13, 13), 9.5f);
                        missile.missileSpeed = Master.Instance.attackerSetup.missileSpeed + Ruleset.Instance.Difficulty.attacker_missileSpeedModifer;
                        missile.onReset += () => --activeMissileRefCounter;
                        missile.GetComponent<CircleCollider>().mask = CollisionMasks.AttackerMissile;
                        missile.GetComponent<CircleCollider>().AddMaskToCheckAgainst(CollisionMasks.DefenderCity);
                        missile.GetComponent<CircleCollider>().onCollided.AddListener(x =>
                        {
                            missile.GetComponent<CircleCollider>().mask = CollisionMasks.Default;
                            if (!missile.interrupted)
                            {
                                missile.interrupted = true;
                                if (x.GetComponent<City>().DamageCityAndDestroyIfZeroHealth(Master.Instance.attackerSetup.missileDamage))
                                {
                                    citySelector.SetFilteredItems(targets);
                                }
                            }
                        });

                        City target = citySelector.Select() as City;
                        missile.Launch(originVector, target.transform.position);
                        ++activeMissileRefCounter;
                        if (--missilesLeft == 0 && Ruleset.Instance.Difficulty.eMode == GameMode.NORMAL)
                        {
                            for (int i = 0; i < Master.Instance.poolManager.pools.Length; ++i)
                            {
                                if (Master.Instance.poolManager.pools[i].type == typeof(Missile))
                                {
                                    missileSelection.Remove(Master.Instance.poolManager.pools[i].template as ExplosiveBase);
                                }
                            }

                            explosiveSelector.SetFilteredItems(missileSelection.ToArray());
                        }
                    }
                    break;

                case nameof(Bomb):
                    {
                        Bomb bomb = Master.Instance.poolManager.GetPoolObj<Bomb>();
                        bomb.onReset += () => --activeMissileRefCounter;
                        Vector2 originVector = new Vector2(randomizer.Next(-13, 13), 9.5f);
                        bomb.Launch(originVector);
                        bomb.GetComponent<CollisionBase>().onCollided.AddListener((x) =>
                        {
                            if (!bomb.interrupted)
                            {
                                bomb.interrupted = true;
                                if (x.GetComponent<City>() != null)
                                {
                                    if (x.GetComponent<City>().DamageCityAndDestroyIfZeroHealth(Master.Instance.attackerSetup.bombDamage))
                                    {
                                        citySelector.SetFilteredItems(targets);
                                    }
                                }
                            }
                        });
                        ++activeMissileRefCounter;
                        if (--bombsLeft == 0 && Ruleset.Instance.Difficulty.eMode == GameMode.NORMAL)
                        {
                            for (int i = 0; i < Master.Instance.poolManager.pools.Length; ++i)
                            {
                                if (Master.Instance.poolManager.pools[i].type == typeof(Bomb))
                                {
                                    missileSelection.Remove(Master.Instance.poolManager.pools[i].template as ExplosiveBase);
                                }
                            }

                            explosiveSelector.SetFilteredItems(missileSelection.ToArray());
                        }
                    }
                    break;
            }
        }
    }
}
