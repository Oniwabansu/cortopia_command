﻿using UnityEngine;

public class TurretMissileControl : MonoBehaviour
{
    private int missileCount;
    [Range(1, 10)]
    public int originalAmmoCount = 10;

    private Vector3 origin, destination;

    public System.Action onMissileFired;
    public System.Action onMissileRecovered;

    private void Awake()
    {
        if (Ruleset.Instance.Difficulty.eMode == GameMode.NORMAL && Ruleset.Instance.Difficulty.defender_recoverMissiles)
        {
            InvokeRepeating("Reload", 5, 5);
        }
    }

    private void Start()
    {
        Reload(originalAmmoCount);
    }

    public bool HasMissilesLeftInStorage()
    {
        return missileCount > 0;
    }

    public bool LaunchMissile()
    {
        try
        {
            if (missileCount == 0) { throw new System.ArgumentOutOfRangeException("No missiles left in turret"); }

            Missile missile = Master.Instance.poolManager.GetPoolObj<Missile>();

            Vector2 mouseOnScreen = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
            missile.missileSpeed = Master.Instance.defenderSetup.missileSpeed;
            missile.GetComponent<CircleCollider>().mask = CollisionMasks.DefenderMissile;
            missile.Launch(transform.position, mouseOnScreen);
            missile.SetupScoreCounter();

            --missileCount;
            onMissileFired?.Invoke();
        }
        catch (System.Exception e)
        {
            Debug.LogWarning(e.Message);
            return false;
        }

        return true;
    }

    public float DistanceToDestination()
    {
        Vector2 mouseOnScreen = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 distanceVector = (Vector2)transform.position - mouseOnScreen;

        return distanceVector.magnitude;
    }

    public void Reload(int count)
    {
        for (int i = 0; i < count; ++i)
        {
            Reload();
        }
    }

    private void Reload()
    {
        if (missileCount < originalAmmoCount)
        {
            missileCount++;
            onMissileRecovered?.Invoke();
        }
    }
}
