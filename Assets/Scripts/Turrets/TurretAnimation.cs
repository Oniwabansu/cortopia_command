﻿using UnityEngine;

public class TurretAnimation : MonoBehaviour
{
    private Transform launcher_arm;

    // Start is called before the first frame update
    void Awake()
    {
        launcher_arm = transform.Find("Rocket launcher");
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 positionOnScreen = Camera.main.WorldToViewportPoint(transform.position);
        Vector2 mouseOnScreen = (Vector2)Camera.main.ScreenToViewportPoint(Input.mousePosition);
        float angle = AngleBetweenTwoPoints(positionOnScreen, mouseOnScreen);

        // rotate the turret base
        transform.rotation = Quaternion.Euler(new Vector3(-90f, -angle - 90f, 0f));

        // begin angle counting down to accomodate for model angle
        if (angle < -90)
        {
            float temp = angle + 90;
            temp *= -2;
            angle += temp;
        }

        // limit angle so the turret won't point downwards
        if (angle <= 0)
        {
            // rotate the turret arm
            launcher_arm.localRotation = Quaternion.Euler(new Vector3(angle, 0, -90f));
        }
    }

    private float AngleBetweenTwoPoints(Vector3 a, Vector3 b)
    {
        return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
    }
}
