﻿using UnityEngine;

public class MissileCluster : MonoBehaviour
{
    public GameObject[] availableStacks;
    public GameObject readyMissile;
    public TurretMissileControl associatedTurret;
    public Floating3DText textComponent;

    private int[] missilesInStack;

    private int missilesPerStack = -1;
    private int nextStackToAffect = -1;

    // Start is called before the first frame update
    void Awake()
    {
        associatedTurret.onMissileFired += HideMissile;
        associatedTurret.onMissileRecovered += ShowMissile;

        missilesInStack = new int[availableStacks.Length];
        for (int i = 0; i < missilesInStack.Length; ++i)
        {
            missilesInStack[i] = 0;
        }
        missilesPerStack = associatedTurret.originalAmmoCount / availableStacks.Length;
    }

    private void HideMissile()
    {
        if (missilesInStack[nextStackToAffect] > 0)
        {
            Destroy(availableStacks[nextStackToAffect].transform.GetChild(availableStacks[nextStackToAffect].transform.childCount - 1).gameObject);
            --missilesInStack[nextStackToAffect];
            nextStackToAffect = nextStackToAffect > 0 ? --nextStackToAffect : availableStacks.Length - 1;
        }
    }

    private void ShowMissile()
    {
        nextStackToAffect = nextStackToAffect < availableStacks.Length - 1 ? ++nextStackToAffect : 0;

        ++missilesInStack[nextStackToAffect];
        GameObject go = Instantiate(readyMissile, availableStacks[nextStackToAffect].transform);
        //go.transform.parent = availableStacks[nextStackToAffect].transform;
        go.transform.localPosition = new Vector2(0, 0.06f * missilesInStack[nextStackToAffect]);
        textComponent.gameObject.SetActive(true);
    }
}
