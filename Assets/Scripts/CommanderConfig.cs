﻿using UnityEngine;

[System.Serializable]
public struct CommanderConfig
{
    [Range(1, 10)]
    public int missileSpeed;

    [Range(1, 50)]
    public int missileDamage;

    [Range(1, 100)]
    public int bombDamage;
}
