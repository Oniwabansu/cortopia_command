﻿using TMPro;
using UnityEngine;

[RequireComponent(typeof(TextMeshProUGUI))]
public class TimeDisplay : MonoBehaviour
{
    private TextMeshProUGUI time_display;

    private float start_time = 0f;

    private void Awake()
    {
        time_display = GetComponent<TextMeshProUGUI>();
        start_time = Time.realtimeSinceStartup;

        if (Ruleset.Instance.Difficulty.eMode != GameMode.CLASSIC)
        {
            transform.parent.gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        time_display.text = ((int)(Time.realtimeSinceStartup - start_time)).ToString();
    }
}
