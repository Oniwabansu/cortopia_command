﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class FadeInOnAwake : MonoBehaviour
{
    CanvasGroup canvasGroup;
    public float fade_duration = 1f;

    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();

        StartFade();
    }

    public void StartFade()
    {
        StartFade(fade_duration);
    }

    public void StartFade(float duration)
    {
        StartCoroutine(FadeOut(duration));
    }

    private IEnumerator FadeOut(float duration)
    {
        float currentTime = 0f;

        while (currentTime < 1f)
        {
            currentTime += Time.deltaTime / duration;
            canvasGroup.alpha = currentTime;

            yield return null;
        }
    }
}
