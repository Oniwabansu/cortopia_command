﻿using TMPro;
using UnityEngine;

[RequireComponent(typeof(TextMeshProUGUI))]
public class ScoreDisplay : MonoBehaviour
{
    private TextMeshProUGUI score_display;

    private void Awake()
    {
        score_display = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        score_display.text = Master.Instance.scoreManager.GetChainScore().ToString();
    }
}
