﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public TypeWriterEffect title_bar;
    public GameObject set_difficulty;

    // Start is called before the first frame update
    void Start()
    {
        title_bar.StartTyping("Cortopia Command", 0.1f);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    public void LaunchGame(int difficulty)
    {
        DifficultyConfig config;
        switch (difficulty)
        {
            case 0:
                config = new DifficultyConfig()
                {
                    attacker_missilesAvailable = 15,
                    attacker_bombsAvailable = 5,
                    attacker_firingSpeedModifier = 5,
                    defender_recoverMissiles = true,
                    pointsModifier = 0.75f,
                    attacker_missileSpeedModifer = 0,
                    eMode = GameMode.NORMAL,
                    receiveMissilesOnScoreModifier = 500,
                    missilesReceived = 0
                };
                break;

            case 1:
            default:
                config = new DifficultyConfig()
                {
                    attacker_missilesAvailable = 15,
                    attacker_bombsAvailable = 5,
                    attacker_firingSpeedModifier = 3,
                    defender_recoverMissiles = false,
                    pointsModifier = 1f,
                    attacker_missileSpeedModifer = 1,
                    eMode = GameMode.NORMAL,
                    receiveMissilesOnScoreModifier = 500,
                    missilesReceived = 0
                };
                break;

            case 2:
                config = new DifficultyConfig()
                {
                    attacker_missilesAvailable = 20,
                    attacker_bombsAvailable = 10,
                    attacker_firingSpeedModifier = 2,
                    defender_recoverMissiles = false,
                    pointsModifier = 1.5f,
                    attacker_missileSpeedModifer = 2,
                    eMode = GameMode.NORMAL,
                    receiveMissilesOnScoreModifier = 500,
                    missilesReceived = 0
                };
                break;

            case 3: // classic game mode
                config = new DifficultyConfig()
                {
                    attacker_missilesAvailable = 1,
                    attacker_bombsAvailable = 1,
                    attacker_firingSpeedModifier = 1,
                    defender_recoverMissiles = false,
                    pointsModifier = 1f,
                    attacker_missileSpeedModifer = 1,
                    eMode = GameMode.CLASSIC,
                    receiveMissilesOnScoreModifier = 400,
                    missilesReceived = 3
                };
                break;
        }
        Ruleset.Instance.SetDifficultyConfig(config);

        SceneManager.LoadScene(1);
    }

    public void LaunchGame()
    {
        set_difficulty.gameObject.SetActive(true);
    }
}
