﻿using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(AudioSource))]
public class PlayAudioOnPointerEnter : MonoBehaviour, IPointerEnterHandler, IPointerClickHandler, IPointerExitHandler
{
    public AudioClip enterClip;
    public AudioClip selectClip;
    public AudioClip exitClip;

    AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (enterClip == null) return;

        audioSource.clip = enterClip;
        if (!audioSource.isPlaying)
        {
            audioSource.Play();
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (selectClip == null) return;

        audioSource.Stop();
        audioSource.clip = selectClip;
        audioSource.Play();
    }

    public void OnPointerExit (PointerEventData eventData)
    {
        if (exitClip == null) return;

        audioSource.clip = exitClip;
        if (!audioSource.isPlaying)
        {
            audioSource.Play();
        }
    }
}
