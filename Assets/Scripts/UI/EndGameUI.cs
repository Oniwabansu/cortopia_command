﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGameUI : MonoBehaviour
{
    public float fadeDuration = 2f;

    public float waitBetweenScores = 1f;

    public CanvasGroup gameOver, scoreText;
    public TextMeshProUGUI cities, chains, time, total;

    private bool canReturnToMainMenu = false;
    

    public void ShowEndGameUI(string victoryText, int totalScore, int citiesLeftScore, int chainAttackScore, int timeScore)
    {
        StartCoroutine(ShowUI(victoryText, totalScore, citiesLeftScore, chainAttackScore, timeScore));
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && canReturnToMainMenu)
        {
            SceneManager.LoadScene(0);
        }
    }

    private IEnumerator ShowUI(string victoryText, int totalScore, int citiesLeftScore, int chainAttackScore, int timeScore)
    {
        gameOver.gameObject.SetActive(true);
        gameOver.GetComponent<TextMeshProUGUI>().text = victoryText;
        yield return FadeCanvasGroup(gameOver, true);

        scoreText.gameObject.SetActive(true);
        StartCoroutine(FadeCanvasGroup(gameOver, false));
        yield return FadeCanvasGroup(scoreText, true);
        gameOver.gameObject.SetActive(false);

        yield return ShowNextScore(cities, $"City Score: {citiesLeftScore}");
        yield return ShowNextScore(chains, $"Chain Hit Score: {chainAttackScore}");
        if (Ruleset.Instance.Difficulty.eMode == GameMode.CLASSIC)
        {
            yield return ShowNextScore(time, $"Survival Score: {timeScore}");
        }
        yield return ShowNextScore(total, $"Total Score: {totalScore}");

        canReturnToMainMenu = true;
    }

    private IEnumerator FadeCanvasGroup(CanvasGroup canvas, bool direction)
    {
        float currentTime = 0f;

        while (currentTime < 1f)
        {
            currentTime += Time.deltaTime / fadeDuration;
            canvas.alpha = direction ? currentTime : 1 - currentTime;

            yield return null;
        }
    }

    private IEnumerator ShowNextScore(TextMeshProUGUI text, string score)
    {
        yield return new WaitForSeconds(waitBetweenScores);
        text.text = score;
        text.gameObject.SetActive(true);
    }
}
