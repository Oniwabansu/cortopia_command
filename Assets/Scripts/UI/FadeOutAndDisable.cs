﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class FadeOutAndDisable : MonoBehaviour
{
    CanvasGroup canvasGroup;

    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    public void StartFade(float duration)
    {
        StartCoroutine(FadeOut(duration));
    }

    private IEnumerator FadeOut(float duration)
    {
        float currentTime = 0f;

        while (currentTime < 1f)
        {
            currentTime += Time.deltaTime / duration;
            canvasGroup.alpha = 1 - currentTime;

            yield return null;
        }

        gameObject.SetActive(false);
    }
}
