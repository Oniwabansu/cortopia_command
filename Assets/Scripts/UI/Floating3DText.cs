﻿using System.Collections;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(TextMeshPro))]
public class Floating3DText : MonoBehaviour
{
    TextMeshPro textComponent;

    private void Awake()
    {
        textComponent = GetComponent<TextMeshPro>();
    }

    private void OnEnable()
    {
        StartFloating();
    }

    public void StartFloating()
    {
        StartCoroutine(FloatText());
    }

    private IEnumerator FloatText()
    {
        float duration = 2f;
        float currentTime = 0f;

        Vector2 originalPosition = transform.position;
        Vector2 endPosition = originalPosition + Vector2.up;

        Color col = textComponent.color;

        while (currentTime < 1f)
        {
            currentTime += Time.deltaTime / duration;

            textComponent.color = col * Mathf.Lerp(0, 1, currentTime);
            transform.position = Vector2.Lerp(originalPosition, endPosition, currentTime);

            yield return null;
        }

        gameObject.SetActive(false);
        transform.position = originalPosition;
    }
}
