﻿using System.Collections;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(TextMeshProUGUI))]
public class TypeWriterEffect : MonoBehaviour
{
    public AudioClip typeWriterKeyPress;

    private TextMeshProUGUI textComponent;
    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        textComponent = GetComponent<TextMeshProUGUI>();
        StartTyping("Cortopia Command", 0.1f);
        if (audioSource != null)
        {
            audioSource.clip = typeWriterKeyPress;
        }
    }

    public void StartTyping(string text, float typingDelay)
    {
        StartCoroutine(TypeWriter(text, typingDelay));
    }

    private IEnumerator TypeWriter(string text, float typingDelay)
    {
        WaitForSeconds wait = new WaitForSeconds(typingDelay);
        System.Text.StringBuilder builder = new System.Text.StringBuilder();
        for (int i = 0; i < text.Length; ++i)
        {
            builder.Append(text[i]);
            textComponent.text = builder.ToString();
            audioSource.Play();
            yield return wait;
        }
    }
}
