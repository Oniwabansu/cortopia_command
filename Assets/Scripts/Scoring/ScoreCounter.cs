﻿public class ScoreCounter
{
    private int referenceCount = 0;
    private int multiplier = 0;

    public void AddRef(int multiplierIncrease)
    {
        referenceCount++;
        multiplier += multiplierIncrease;
    }

    public void RemoveRef()
    {
        if (--referenceCount <= 0)
        {
            SubmitMultiplier();
        }
    }

    private void SubmitMultiplier()
    {
        Master.Instance.scoreManager.AddToChainedScoreCounter(FactorialFromMultiplier());
    }

    private int FactorialFromMultiplier()
    {
        int factorial = 1;
        for (int i = 1; i < multiplier + 1; ++i)
        {
            factorial *= i;
        }
        
        return factorial;
    }
}
