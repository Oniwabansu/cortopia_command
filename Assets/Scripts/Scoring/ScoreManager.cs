﻿using UnityEngine;

public class ScoreManager
{
    private int chainedScoreModifierForMissiles = 0;
    private int chainedScoreCounter = 0;
    private int cityScoreCounter = 0;

    public int cityValue = 1000;
    public int cityLossValue = 1500;

    private float start_time = 0f;

    public ScoreManager()
    {
        cityValue = (int)(cityValue * Ruleset.Instance.Difficulty.pointsModifier);
        cityLossValue = (int)(cityLossValue * Ruleset.Instance.Difficulty.pointsModifier);

        start_time = Time.realtimeSinceStartup;
    }

    public void RemoveFromChainedScoreCounter(int score)
    {
        chainedScoreCounter -= (int)(score * Ruleset.Instance.Difficulty.pointsModifier);
    }

    public void AddToChainedScoreCounter(int score)
    {
        chainedScoreCounter += (int)(score * Ruleset.Instance.Difficulty.pointsModifier);
        if (Ruleset.Instance.Difficulty.eMode == GameMode.CLASSIC)
        {
            chainedScoreModifierForMissiles += (int)(score * Ruleset.Instance.Difficulty.pointsModifier);
            if (chainedScoreModifierForMissiles >= Ruleset.Instance.Difficulty.receiveMissilesOnScoreModifier)
            {
                chainedScoreModifierForMissiles -= Ruleset.Instance.Difficulty.receiveMissilesOnScoreModifier;
                Master.Instance.turretCommand.ReceiveMissiles();
            }
        }
    }

    public void AddCityToScore()
    {
        cityScoreCounter += cityValue;
    }

    public void RemoveCityFromScore()
    {
        cityScoreCounter -= cityLossValue;
    }

    public int GetCityScore()
    {
        return cityScoreCounter;
    }

    public int GetTotalScore()
    {
        return GetCityScore() + GetChainScore() + GetTimeScore();
    }

    public int GetTimeScore()
    {
        if (Ruleset.Instance.Difficulty.eMode == GameMode.CLASSIC)
        {
            return (int)(Time.realtimeSinceStartup - start_time) * 10;
        }

        return 0;
    }

    public int GetChainScore()
    {
        return chainedScoreCounter;
    }
}
