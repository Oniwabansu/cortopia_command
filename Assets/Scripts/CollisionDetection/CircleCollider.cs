﻿using UnityEngine;

public class CircleCollider : CollisionBase
{
    public float radius;
    
    private void Update()
    {
        if (collisionMask == 0) { return; }

        CircleCollider[] collisionCircles = FindObjectsOfType<CircleCollider>();
        collisionCircles = System.Array.FindAll(collisionCircles, x => ((int)x.mask & collisionMask) != 0 && x.enabled);

        for (int i = 0; i < collisionCircles.Length; ++i)
        {
            if (CheckCollisionRadius(collisionCircles[i]))
            {
                onCollided?.Invoke(collisionCircles[i]);
            }
        }

        RectCollider[] rectColliders = FindObjectsOfType<RectCollider>();
        rectColliders = System.Array.FindAll(rectColliders, x => ((int)x.mask & collisionMask) != 0 && x.enabled);

        for (int i = 0; i < rectColliders.Length; ++i)
        {
            if (CheckCollisionIntersection(rectColliders[i]))
            {
                onCollided?.Invoke(rectColliders[i]);
            }
        }
    }

    /// <summary>
    /// Checks if the missile has collided with any other missiles
    /// </summary>
    /// <param name="radius">The radius to check for this missile</param>
    /// <param name="other">The missile to check against</param>
    /// <returns>true if the radii overlap, otherwise false</returns>
    private bool CheckCollisionRadius(CircleCollider other)
    {
        float dx = this.transform.position.x - other.transform.position.x;
        float dy = this.transform.position.y - other.transform.position.y;
        float radii = this.radius + other.radius;

        return (dx * dx) + (dy * dy) < radii * radii;
    }

    private bool CheckCollisionIntersection(RectCollider rectCollider)
    {
        float dx = Mathf.Abs(transform.position.x - rectCollider.rect.x);
        float dy = Mathf.Abs(transform.position.y - rectCollider.rect.y);

        float halfRectWidth = rectCollider.rect.width / 2;
        float halfRectHeight = rectCollider.rect.height / 2;
        if (dx > (halfRectWidth + radius)) { return false; }
        if (dy > (halfRectHeight + radius)) { return false; }

        if (dx <= halfRectWidth) { return true; }
        if (dy <= halfRectHeight) { return true; }

        float dx2 = (dx - halfRectWidth) * (dx - halfRectWidth);
        float dy2 = (dy - halfRectHeight) * (dy - halfRectHeight);
        float cornerDistance_sq = dx2 * dx2 + dy2 * dy2;

        return (cornerDistance_sq <= (radius * radius));
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}