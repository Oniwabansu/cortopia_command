﻿using UnityEngine;

public class RectCollider : CollisionBase
{
    public Rect rect;

    // Update is called once per frame
    // only checking against circle colliders currently as we only need one instance of a box collider for now
    void Update()
    {
        if (collisionMask == 0) { return; }

        CircleCollider[] collisionCircles = FindObjectsOfType<CircleCollider>();
        collisionCircles = System.Array.FindAll(collisionCircles, x => ((int)x.mask & collisionMask) != 0 && x.enabled);

        for (int i = 0; i < collisionCircles.Length; ++i)
        {
            if (CheckCollisionIntersection(collisionCircles[i]))
            {
                onCollided?.Invoke(collisionCircles[i]);
            }
        }
    }

    private bool CheckCollisionIntersection(CircleCollider circle)
    {
        float dx = Mathf.Abs(circle.transform.position.x - rect.x);
        float dy = Mathf.Abs(circle.transform.position.y - rect.y);

        float halfRectWidth = rect.width / 2;
        float halfRectHeight = rect.height / 2;
        if (dx > (halfRectWidth + circle.radius)) { return false; }
        if (dy > (halfRectHeight + circle.radius)) { return false; }

        if (dx <= halfRectWidth) { return true; }
        if (dy <= halfRectHeight) { return true; }

        float dx2 = (dx - halfRectWidth) * (dx - halfRectWidth);
        float dy2 = (dy - halfRectHeight) * (dy - halfRectHeight);
        float cornerDistance_sq = dx2 * dx2 + dy2 * dy2;

        return (cornerDistance_sq <= (circle.radius * circle.radius));
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(new Vector2(rect.x, rect.y), new Vector2(rect.width / 2, rect.height / 2));
    }
}
