﻿using UnityEngine;

public class CollisionBase : MonoBehaviour
{
    public CollisionMasks mask = CollisionMasks.Default;
    protected int collisionMask = 0;

    public CollisionEvent onCollided = new CollisionEvent();

    public void AddMaskToCheckAgainst(CollisionMasks mask)
    {
        collisionMask |= (int)mask;
    }

    public void RemoveMaskFromChecks(CollisionMasks mask)
    {
        collisionMask &= ~(int)mask;
    }
}
