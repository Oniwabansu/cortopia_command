﻿public enum CollisionMasks
{
    Default             = 1 << 0,
    AttackerMissile     = 1 << 1,
    DefenderMissile     = 1 << 2,
    DefenderCity        = 1 << 3,
    MissileExplosion    = 1 << 4,
    Ground              = 1 << 5
}