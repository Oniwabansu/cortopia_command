﻿using UnityEngine;

[RequireComponent(typeof(CircleCollider))]
public class City : MonoBehaviour, IWeightedItem
{
    [SerializeField]
    [Range(1, 200)]
    private int health = 100;

    public int selectionWeight = 0;

    public System.Action onDestroyed;

    private void Awake()
    {
        SetWeight(20);

        float baseCityCollisionRadius = Mathf.Sqrt(GetComponent<MeshFilter>().sharedMesh.bounds.extents.x + GetComponent<MeshFilter>().sharedMesh.bounds.extents.y);

        GetComponent<CircleCollider>().radius = baseCityCollisionRadius;
    }

    private void Start()
    {
        Master.Instance.scoreManager.AddCityToScore();
    }

    public bool DamageCityAndDestroyIfZeroHealth(int damage)
    {
        if (health <= 0) { return false; }

        health -= damage;

        if (health <= 0)
        {
            SetWeight(5);
            DestroyCity();
            return true;
        }

        return false;
    }

    private void DestroyCity()
    {
        Debug.Log("Boom");
        onDestroyed?.Invoke();
        GetComponent<Renderer>().enabled = false;
        GetComponent<CircleCollider>().enabled = false;
        Master.Instance.scoreManager.RemoveCityFromScore();
    }

    public void SetWeight(int selectionWeight)
    {
        this.selectionWeight = selectionWeight;
    }

    public int GetWeight()
    {
        return selectionWeight;
    }
}
