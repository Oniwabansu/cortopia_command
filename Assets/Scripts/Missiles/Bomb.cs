﻿using UnityEngine;

[RequireComponent(typeof(CircleCollider))]
public class Bomb : ExplosiveBase
{
    System.Random randomizer = new System.Random();

    private void Awake()
    {
        explosionTransform = transform.Find("explosion");
        float baseBombRadius = Mathf.Sqrt(GetComponent<MeshFilter>().sharedMesh.bounds.extents.x + GetComponent<MeshFilter>().sharedMesh.bounds.extents.y);

        GetComponent<CircleCollider>().radius = baseBombRadius * transform.localScale.x;
        GetComponent<CircleCollider>().AddMaskToCheckAgainst(CollisionMasks.DefenderCity);
        GetComponent<CircleCollider>().AddMaskToCheckAgainst(CollisionMasks.Ground);
    }

    private void OnEnable()
    {
        GetComponent<CollisionBase>().onCollided.AddListener((x) => GetComponent<ProjectileMovement>().Interrupt());
        onExploded = () => explosionTransform.gameObject.SetActive(true);
        onExploded += Explode;
    }

    private void OnDisable()
    {
        GetComponent<CircleCollider>().onCollided = new CollisionEvent();
    }

    private void Update()
    {
        if (interrupted)
        {
            GetComponent<ProjectileMovement>().Interrupt();
        }
    }

    public void Launch(Vector2 initialPos)
    {
        // randomize the angle slightly from purely downwards
        float angle = 270 + randomizer.Next(-50, 50);
        // create a random float for the velocity between 0.1 and 1, gravity will take care of the rest
        float velocity = randomizer.Next(1, 2) / randomizer.Next(5, 10);

        GetComponent<ProjectileMovement>().Launch(initialPos, angle, velocity, onExploded);
    }

    protected override void ResetExplosive()
    {
        base.ResetExplosive();

        hasAddedScore = false;

        Master.Instance.poolManager.ReturnToPool<Bomb>(this);
    }

    bool hasAddedScore = false;

    public override void TransferScoreCounter(ScoreCounter scoreCounter)
    {
        if (!hasAddedScore)
        {
            hasAddedScore = true;
            this.scoreCounter = scoreCounter;
            this.scoreCounter?.AddRef(4);
        }
    }
}
