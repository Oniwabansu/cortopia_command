﻿using UnityEngine;

public abstract class ExplosiveBase : PoolableObject, IWeightedItem
{
    public int selectionWeight = 0;

    [SerializeField]
    [Range(1, 15)]
    protected int explosionRadiusMax = 10;
    
    public bool hasExploded = false;
    public bool interrupted = false;

    protected Transform explosionTransform = null;

    public System.Action onExploded;
    public System.Action onReset;

    public ScoreCounter scoreCounter
    {
        get;
        protected set;
    }

    public abstract void TransferScoreCounter(ScoreCounter scoreCounter);
    public void SetupScoreCounter()
    {
        scoreCounter = new ScoreCounter();
    }

    protected virtual void ResetExplosive()
    {
        if (scoreCounter != null)
        {
            scoreCounter.RemoveRef();
            scoreCounter = null;
        }

        onReset?.Invoke();
        onReset = null;

        hasExploded = false;
        interrupted = false;
        GetComponent<CircleCollider>().enabled = true;
    }

    public void SetWeight(int weight)
    {
        this.selectionWeight = weight;
    }

    public int GetWeight()
    {
        return selectionWeight;
    }

    protected void Explode()
    {
        if (hasExploded) { return; }
        hasExploded = true;

        GetComponent<CircleCollider>().enabled = false;
        StartCoroutine(explosionTransform.GetComponent<MissileExplosion>().Explode(explosionRadiusMax, ResetExplosive));
    }
}
