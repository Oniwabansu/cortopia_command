﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(CircleCollider))]
public class MissileExplosion : MonoBehaviour
{
    private CircleCollider colliderReference;

    private void Awake()
    {
        colliderReference = GetComponent<CircleCollider>();
        colliderReference.mask = CollisionMasks.MissileExplosion;
    }

    private void OnEnable()
    {
        colliderReference.AddMaskToCheckAgainst(CollisionMasks.AttackerMissile);

        colliderReference.onCollided.AddListener(x =>
        {
            if (x.GetComponent<ExplosiveBase>() != null && !x.GetComponent<ExplosiveBase>().hasExploded)
            {
                x.GetComponent<ExplosiveBase>().interrupted = true;
                x.GetComponent<ExplosiveBase>().TransferScoreCounter(transform.parent.GetComponent<ExplosiveBase>().scoreCounter);
            }
        });
    }

    private void OnDisable()
    {
        colliderReference.onCollided = new CollisionEvent();
        colliderReference.enabled = false;
        colliderReference.RemoveMaskFromChecks(CollisionMasks.AttackerMissile);
    }

    // Update is called once per frame
    void Update()
    {
        // get the radius of the explosion from the scale of the explosion and take into account the missile scale
        colliderReference.radius = (transform.localScale.x / 2) * transform.parent.localScale.x;
    }

    public IEnumerator Explode(int explosionRadiusMax, System.Action onExplosionFinished)
    {
        colliderReference.enabled = true;

        Vector3 explosionStart = transform.localScale;
        Vector3 explosionEnd = new Vector3(explosionRadiusMax, explosionRadiusMax, 0.1f);
        float duration = 4f;
        float currentTime = 0f;
        while (currentTime < 1f)
        {
            // use sin to scale up and then down again
            float t = Mathf.Sin(Mathf.PI * currentTime);

            transform.localScale = Vector3.Lerp(explosionStart, explosionEnd, t);
            currentTime += Time.deltaTime / duration;

            yield return null;
        }
        
        onExplosionFinished?.Invoke();
        gameObject.SetActive(false);
    }
}
