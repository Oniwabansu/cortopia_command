﻿using UnityEngine;

[RequireComponent(typeof(CircleCollider))]
public class Missile : ExplosiveBase
{
    [Range(1, 10)]
    public int missileSpeed = 5;
    
    private void Awake()
    {
        // using width value for both width and height since the missile is a cube
        float squaredHalfWidth = Mathf.Pow(transform.localScale.x / 2, 2);
        float baseMissileCollisionRadius = Mathf.Sqrt(squaredHalfWidth + squaredHalfWidth);

        GetComponent<CircleCollider>().radius = baseMissileCollisionRadius;

        explosionTransform = transform.Find("explosion");
    }

    private void OnEnable()
    {
        onExploded = () => explosionTransform.gameObject.SetActive(true);
        onExploded += Explode;
    }

    private void OnDisable()
    {
        GetComponent<CircleCollider>().onCollided = new CollisionEvent();
    }

    private void Update()
    {
        if (interrupted)
        {
            GetComponent<LerpedMovement>().Interrupt();
        }
    }

    public void Launch(Vector2 origin, Vector2 destination)
    {
        GetComponent<LerpedMovement>().Launch(origin, destination, missileSpeed, onExploded);
    }
    
    protected override void ResetExplosive()
    {
        base.ResetExplosive();

        transform.Find("TrailRenderer").GetComponent<TrailRenderer>().Clear();

        Master.Instance.poolManager.ReturnToPool<Missile>(this);
    }

    public override void TransferScoreCounter(ScoreCounter scoreCounter)
    {
        this.scoreCounter = scoreCounter;
        this.scoreCounter?.AddRef(2);
    }
}
