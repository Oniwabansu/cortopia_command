﻿using UnityEngine;

public enum GameMode
{
    CLASSIC,
    NORMAL
}

public struct DifficultyConfig
{
    public int attacker_missilesAvailable;
    public int attacker_bombsAvailable;
    public int attacker_firingSpeedModifier;
    public int attacker_missileSpeedModifer;
    public bool defender_recoverMissiles;
    public float pointsModifier;
    public GameMode eMode;

    public int receiveMissilesOnScoreModifier;
    public int missilesReceived;
}

public class Ruleset : MonoBehaviour
{
    public static Ruleset Instance { get; private set; }

    public DifficultyConfig Difficulty
    {
        get;
        private set;
    }

    private void Awake()
    {
        try
        {
            if (Instance != null) { throw new System.SystemException("Can't create more than one instance of Difficulty Setting"); }

            Instance = this;
            DontDestroyOnLoad(this);
        }
        catch (System.Exception e)
        {
            Debug.LogError(e.Message);
            Destroy(this);
        }
    }

    public void SetDifficultyConfig(DifficultyConfig config)
    {
        this.Difficulty = config;
    }
}
