﻿using System.Collections;
using UnityEngine;

public class TurretCommand : MonoBehaviour
{
    public TurretMissileControl[] launchPads;

    public GameObject outOfAmmoCanvas;
    private Coroutine outOfAmmoCoroutine = null;
    private void Awake()
    {
        if (launchPads == null || launchPads.Length == 0)
        {
            throw new System.NullReferenceException("Please ensure launch pads have been assigned");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!Master.Instance.GameActive) { return; }

        if (Input.GetMouseButtonDown(0))
        {
            float closestDistance = float.MaxValue;
            int selectedLaunchPad = -1;
            // loop through the launch pads and find the closest available turret
            for (int i = 0; i < launchPads.Length; ++i)
            {
                if (launchPads[i].DistanceToDestination() < closestDistance)
                {
                    if (launchPads[i].HasMissilesLeftInStorage())
                    {
                        closestDistance = launchPads[i].DistanceToDestination();
                        selectedLaunchPad = i;
                    }
                }
            }

            if (selectedLaunchPad != -1)
            {
                launchPads[selectedLaunchPad].LaunchMissile();
            }
            else
            {
                Debug.LogWarning("Out of available missiles");
                if (outOfAmmoCoroutine == null)
                {
                    outOfAmmoCoroutine = StartCoroutine(FlashOutOfAmmo());
                }
            }
        }
    }

    private IEnumerator FlashOutOfAmmo()
    {
        WaitForSeconds flash_speed = new WaitForSeconds(0.5f);
        int flashCount = 3;

        while (flashCount != 0)
        {
            --flashCount;
            outOfAmmoCanvas.SetActive(true);
            yield return flash_speed;

            outOfAmmoCanvas.SetActive(false);
            yield return flash_speed;
        }

        outOfAmmoCoroutine = null;
    }

    public void ReceiveMissiles()
    {
        for (int i = 0; i < launchPads.Length; ++i)
        {
            launchPads[i].Reload(Ruleset.Instance.Difficulty.missilesReceived);
        }
    }
}
