﻿using System;
using System.Collections.Generic;

class MultiSelector : BaseSelector
{
    internal MultiSelector(WeightedSelector weightedSelector) : base(weightedSelector)
    {

    }

    internal IWeightedItem[] Select(IWeightedItem[] weightedItems, int count)
    {
        Validate(weightedItems, count);

        List<IWeightedItem> filterCopy = new List<IWeightedItem>(weightedItems);
        List<IWeightedItem> ResultList = new List<IWeightedItem>();

        do
        {
            IWeightedItem item = null;
            if (weightedSelector.options.AllowDuplicates)
            {
                item = ExecuteSelect(weightedItems);
            }
            else
            {
                item = ExecuteSelectWithLinearSearch(weightedItems);
            }

            ResultList.Add(item);

            if (!weightedSelector.options.AllowDuplicates)
            {
                filterCopy.Remove(item);
            }
        } while (ResultList.Count < count);

        return ResultList.ToArray();
    }

    private void Validate(IWeightedItem[] weightedItems, int count)
    {
        if (count <= 0)
        {
            throw new InvalidOperationException("Count must be > 0.");
        }

        if (weightedItems == null || weightedItems.Length == 0)
        {
            throw new ArgumentOutOfRangeException("weighted items array is null or empty");
        }

        if (weightedSelector.options.AllowDuplicates && weightedItems.Length == count)
        {
            throw new InvalidOperationException("Not enough items in filter to select " + count);
        }
    }
}