﻿using System;

public sealed class WeightedSelector
{
    public readonly SelectorOptions options;

    private IWeightedItem[] weightedItems = null;

    internal int[] CumulativeWeights = null;
    private bool IsCumulativeWeightsStale;

    public WeightedSelector(SelectorOptions options = null)
    {
        if (options == null)
        {
            options = new SelectorOptions();
        }

        this.options = options;
        IsCumulativeWeightsStale = false;
    }

    public void SetFilteredItems(IWeightedItem[] newFilters)
    {
        if (options.DropZeroWeightItems)
        {
            weightedItems = Array.FindAll(newFilters, x => x.GetWeight() != 0);
        }
        else
        {
            weightedItems = newFilters;
        }
        IsCumulativeWeightsStale = true;
    }

    public IWeightedItem Select()
    {
        CalculateCumulativeWeights();

        var Selector = new SingleSelector(this);
        return Selector.Select(weightedItems);
    }

    public IWeightedItem[] SelectMultiple(int count)
    {
        CalculateCumulativeWeights();

        var Selector = new MultiSelector(this);
        return Selector.Select(weightedItems, count);
    }

    private void CalculateCumulativeWeights()
    {
        if (!IsCumulativeWeightsStale) { return; }

        IsCumulativeWeightsStale = false;
        CumulativeWeights = BinarySearchOptimizer.GetCumulativeWeights(weightedItems);
    }
}