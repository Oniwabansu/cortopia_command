﻿using System;

internal abstract class BaseSelector
{
    protected readonly WeightedSelector weightedSelector;
    private readonly Random _randomizer;

    internal BaseSelector(WeightedSelector weightedSelector)
    {
        this.weightedSelector = weightedSelector;
        _randomizer = new Random();
    }

    private int GetSeed(IWeightedItem[] weightedItems)
    {
        int topRange = 1;

        for (int i = 0; i < weightedItems.Length; ++i)
        {
            topRange += (int)weightedItems[i].GetWeight();
        }

        return _randomizer.Next(1, topRange);
    }

    protected IWeightedItem ExecuteSelect(IWeightedItem[] weightedItems)
    {
        if (weightedItems.Length == 0)
        {
            throw new InvalidOperationException("Tried to do a select, but WeightedItems was empty.");
        }

        int seed = GetSeed(weightedItems);

        return BinarySearch(weightedItems, seed);
    }

    protected IWeightedItem ExecuteSelectWithLinearSearch(IWeightedItem[] weightedItems)
    {
        if (weightedItems.Length == 0)
        {
            throw new InvalidOperationException("Tried to do a select, but WeightedItems was empty.");
        }

        var seed = GetSeed(weightedItems);

        return LinearSearch(weightedItems, seed);
    }

    private IWeightedItem LinearSearch(IWeightedItem[] weightedItems, int seed)
    {
        int runningWeight = 0;

        for (int i = 0; i < weightedItems.Length; ++i)
        {
            runningWeight += (int)weightedItems[i].GetWeight();
            if (seed <= runningWeight)
            {
                return weightedItems[i];
            }
        }

        throw new InvalidOperationException("Failed to select advert, filter left empty");
    }

    private IWeightedItem BinarySearch(IWeightedItem[] weightedItems, int seed)
    {
        int index = Array.BinarySearch(weightedSelector.CumulativeWeights, seed);

        if (index < 0)
        {
            index = (index * -1) - 1;
        }

        return weightedItems[index];
    }
}