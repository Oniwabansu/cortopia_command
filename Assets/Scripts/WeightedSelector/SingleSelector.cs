﻿class SingleSelector : BaseSelector
{
    internal SingleSelector(WeightedSelector weightedSelector) : base(weightedSelector)
    {

    }

    internal IWeightedItem Select(IWeightedItem[] weightedItems)
    {
        if (weightedItems == null || weightedItems.Length == 0)
        {
            throw new System.ArgumentOutOfRangeException("weighted item array is null or empty");
        }

        return ExecuteSelect(weightedItems);
    }
}