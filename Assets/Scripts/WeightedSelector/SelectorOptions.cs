﻿[System.Serializable]
public class SelectorOptions
{
    public bool AllowDuplicates;
    public bool DropZeroWeightItems;

    public SelectorOptions()
    {
        AllowDuplicates = false;
        DropZeroWeightItems = true;
    }
}