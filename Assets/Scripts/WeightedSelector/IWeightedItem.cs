﻿public interface IWeightedItem
{
    void SetWeight(int weight);
    int GetWeight();
}