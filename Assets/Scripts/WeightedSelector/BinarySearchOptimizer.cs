﻿class BinarySearchOptimizer
{
    public static int[] GetCumulativeWeights(IWeightedItem[] items)
    {
        if (items.Length == 0) { throw new System.ArgumentOutOfRangeException("Weighted items array is empty"); }

        int RunningWeight = 0;
        var ResultArray = new int[items.Length + 1];

        for (int i = 0; i < items.Length; ++i)
        {
            RunningWeight += items[i].GetWeight();
            ResultArray[i] = RunningWeight;
        }

        return ResultArray;
    }
}